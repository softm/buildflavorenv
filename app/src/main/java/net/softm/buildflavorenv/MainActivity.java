package net.softm.buildflavorenv;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String envStr = "";
        if ( BuildConfig.IS_ORIGIN ) {
            envStr = "진짜-" + BuildConfig.BUILD_TYPE.toUpperCase();
        } else {
            envStr = "가짜-" + BuildConfig.BUILD_TYPE.toUpperCase();
        }

        if ( Utils.devMode.equals(Utils.EnumDevMode.DEBUG) ) {
            envStr += System.getProperty("line.separator") + "디버그";
        } else if ( Utils.devMode.equals(Utils.EnumDevMode.LOCAL) ) {
            envStr += System.getProperty("line.separator") + "로컬";

        } else if ( Utils.devMode.equals(Utils.EnumDevMode.DEV) ) {
            envStr += System.getProperty("line.separator") + "개발";

        } else if ( Utils.devMode.equals(Utils.EnumDevMode.RELEASE_TEST) ) {
            envStr += System.getProperty("line.separator") + "운영_디버그";

        } else if ( Utils.devMode.equals(Utils.EnumDevMode.RELEASE) ) {
            envStr += System.getProperty("line.separator") + "운영";

        }

        envStr += System.getProperty("line.separator") + Utils.devMode;
        TextView tv = (TextView) findViewById(R.id.tx_env);
        tv.setText(envStr);
        Toast.makeText(this,envStr,Toast.LENGTH_SHORT).show();

    }
}
