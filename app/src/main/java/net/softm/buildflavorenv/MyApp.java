package net.softm.buildflavorenv;

import android.app.Application;

public class MyApp extends Application {
    static {
        Utils.devMode = Utils.EnumDevMode.valueOf(BuildConfig.BUILD_TYPE.toUpperCase());
    }

    private final String TAG = this.getClass().getSimpleName();

    public static boolean isTest = Utils.devMode.equals(Utils.EnumDevMode.DEV) || Utils.devMode.equals(Utils.EnumDevMode.DEBUG) ?true:false;

    public static boolean isDebug = isTest || Utils.devMode.equals(Utils.EnumDevMode.RELEASE_TEST)?true:false;

}
